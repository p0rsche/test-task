import { Pipe, PipeTransform } from '@angular/core'

import {Client} from '../shared/client'

@Pipe({name: 'fullName'})
export class FullNamePipe implements PipeTransform {
  transform(client: Client): string {
    return client.general.firstName + ' ' + client.general.lastName
  }
}
