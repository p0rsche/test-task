import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { Client } from './shared/client';
import { ClientApiService } from './services/client-api.service';

@Component({
  selector: 'direct-services',
  styles: [`
    body {
      padding-top:30px;
    }
    .clients-container {
      max-height: 500px;
      overflow: auto;
    }
  `],
  template: `
  <div class="container">
    <div class="row well">
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 clients-container">
        <div class="form-group">
          <input
            name="filteredClients"
            [formControl]="filterString"
            type="text"
            class="form-control"
            placeholder="Filter clients..."
          >
        </div>
        <clients-container (selected)="onClientSelect($event)" [clients]="filteredClients || clients"></clients-container>
      </div>
      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 client-details">
        <client-details [client]="selectedClient"></client-details>
      </div>
    </div>
  </div>
  `,
  encapsulation: ViewEncapsulation.None
})
export class DirectServicesComponent  {
  filterString = new FormControl()

  clients: Client[]
  filteredClients: Client[] = null
  selectedClient: Client

  constructor(private clientService: ClientApiService){

  }

  ngOnInit(){
    this.clientService
      .getClients()
      .subscribe(clients => {
        this.clients = clients.map((client:any) => {
          client['_index'] = this.objToString(client)
          return client
        })
      })
    this.filterString.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .map(filterString => filterString.toLowerCase())
      .subscribe(filterString => this.filterClients(filterString))
  }

  onClientSelect(client: Client) {
    this.selectedClient = client
  }

  filterClients(filter: string): any {
    this.filteredClients = this.clients.filter(client => client['_index'].includes(filter))
  }

  objToString (obj:any):any {
    let flattenedObject = this.flatten(obj)
    return Object.values(flattenedObject).join(' ').toLowerCase()
  }

  private flatten(o:any):any {
    return Object.assign(
      {},
      ...function _flatten(o:any):any {
        return [].concat(...Object.keys(o)
          .map(k =>
            typeof o[k] === 'object' ?
              _flatten(o[k]) :
              ({[k]: o[k]})
          )
        );
      }(o)
    )
  }


}
