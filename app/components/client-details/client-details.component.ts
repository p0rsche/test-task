import { Component, Input } from '@angular/core'
import { Client } from '../../shared/client'

@Component({
  selector: 'client-details',
  styles: [`
    .glyphicon {
      margin-bottom: 10px;
      margin-right: 10px;
    }

    small {
      display: block;
      line-height: 1.428571429;
      color: #999;
    }
  `],
  templateUrl: 'app/components/client-details/client-details.component.html'
})
export class ClientDetailsComponent {
  @Input() client: Client
}
