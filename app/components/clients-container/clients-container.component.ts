import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Client } from '../../shared/client'

@Component({
  selector: 'clients-container',
  styles: [`
    .user-row {
      margin-bottom: 14px;
      cursor: pointer;
    }
    .user-row:last-child {
      margin-bottom: 0;
    }
    .user-avatar-small {
      width: 50px;
      height: auto;
    }
    .user-avatar-large {
      width: 380px;
      height: auto;
    }
  `],
  template:`
  <div
    *ngFor="let client of clients"
    class="row user-row"
    [class.bg-success]="client === selectedClient"
    (click)="onClientSelect(client)"
    >
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
      <img class="img-circle user-avatar-small"
      src="{{client.general.avatar}}"
      alt="{{client.fullName}}">
    </div>
    <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
      <strong>{{client | fullName}}</strong><br>
      <span class="text-muted">{{client.job.title}}</span>
    </div>
  </div>
  `
})
export class ClientsContainerComponent {
  @Input() clients: Client[]
  @Output() selected = new EventEmitter()
  selectedClient: Client

  onClientSelect(client: Client) {
    this.selectedClient = client
    this.selected.next(this.selectedClient)
  }
}
