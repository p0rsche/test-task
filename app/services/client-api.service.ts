import { Injectable } from '@angular/core'
import { Headers, Http, Response, URLSearchParams, Jsonp} from '@angular/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/Rx'
import 'rxjs/add/observable/throw'

@Injectable()
export class ClientApiService {
  headers: Headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })

  clients_url: string = 'clients.json'

  constructor(private http: Http) {

  }

  private getJson(response: Response) {
    return response.json()
  }

  private errorCheck(response: Response):Response {
    if(response.status >= 200 && response.status < 300) {
      return response
    } else {
      const error = new Error(response.statusText)
      console ? console.error(error): void 0
      throw error
    }
  }

  getClients():Observable<any> {
    return this.http
      .get(`${this.clients_url}`, this.headers)
      .map(this.errorCheck).catch(err => Observable.throw(err))
      .map(this.getJson)
  }

  filter(filter: string) {

  }
}
