//modules
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http'
import { ReactiveFormsModule } from "@angular/forms";
//components
import { DirectServicesComponent }  from './direct-services.component';
import { ClientsContainerComponent }  from './components/clients-container/clients-container.component';
import { ClientDetailsComponent }  from './components/client-details/client-details.component';
//services, directives, providers
import { ClientApiService } from './services/client-api.service'
//pipes
import { FullNamePipe } from './pipes/fullname.pipe'

@NgModule({
  imports:      [ BrowserModule, HttpModule, ReactiveFormsModule ],
  declarations: [
    DirectServicesComponent ,
    ClientsContainerComponent,
    ClientDetailsComponent,
    FullNamePipe
  ],
  providers: [ClientApiService],
  bootstrap:    [ DirectServicesComponent ]
})
export class AppModule { }
